<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route url
Route::get('/', function() {
  return view('index');
});

Route::get('/dashboard', function() {
  return view('pages.dashboard');
})->middleware('check.auth');

Route::get('/admin', function() {
  return view('pages.login');
})->name('login');

Route::get('/logout', 'DashboardController@logout')->name('logout')->middleware('check.auth');

Route::post('/login', 'DashboardController@login');
