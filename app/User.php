<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    public $timestamps = false;

    public function login($username, $password)
    {
      $user = $this->where('username', $username)->first();

      if(!$user)
        return 0;

      if($user->password != $password)
        return 0;

      return 1;
    }
}
