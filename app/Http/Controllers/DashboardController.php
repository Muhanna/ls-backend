<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use App\User;

class DashboardController extends Controller
{
    function __construct(User $user)
    {
      $this->user = $user;
    }

    public function login(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'username' => ['required', 'string'],
        'password' => ['required', 'string']
      ]);

      if($validator->fails())
        return response()->json(['success' => 0, 'error' => 'You sent invalid credential']);

      $username = $request->username;
      $password = $request->password;

      $result = $this->user->login($username, $password);

      if(!$result)
        return response()->json(['success' => 0, 'error' => 'You sent invalid credential'], 200);

      session(['username' => $username]);
      return response()->json(['success' => 1], 200);
    }

    public function logout(Request $request)
    {
      $request->session()->flush();
      return redirect()->route('login');
    }
}

