<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Post;

class PostController extends Controller
{
    function __construct(Post $post)
    {
      $this->post = $post;
    }

    public function getAll()
    {
      $posts = $this->post->all();

      return response()->json(['data' => $posts], 200);
    }

    public function create(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'title_english' => ['required', 'string', 'max:255'],
        'content_english' => ['required', 'string', 'max:255'],
        'title_arabic' => ['required', 'string', 'max:255'],
        'content_arabic' => ['required', 'string', 'max:255'],
        'image' => ['required','file', 'mimes:jpeg,jpg,png']
      ]);

      if($validator->fails())
        return response()->json([
          'success' => 0,
          'data' => $validator->errors()->all()
        ]);

      $post = new Post;
      $post->title_english = $request->title_english;
      $post->content_english = $request->content_english;
      $post->title_arabic = $request->title_arabic;
      $post->content_arabic = $request->content_arabic;
      $post->image = basename($request->image->store('public/upload/posts'));
      $post->save();

      return response()->json(['success' => 1], 200);
    }

    public function edit(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'id' => ['required'],
        'title_english' => ['required', 'string', 'max:255'],
        'content_english' => ['required', 'string', 'max:255'],
        'title_arabic' => ['required', 'string', 'max:255'],
        'content_arabic' => ['required', 'string', 'max:255'],
        'image' => ['nullable','file', 'mimes:jpeg,jpg,png']
      ]);

      if($validator->fails())
        return response()->json([
          'success' => 0,
          'data' => $validator->errors()->all()
        ]);

      $id = $request->id;
      $title_english = $request->title_english;
      $content_english = $request->content_english;
      $title_arabic = $request->title_arabic;
      $content_arabic = $request->content_arabic;
      $image = !!$request->image ? basename($request->image->store('/public/upload/posts')) : null;

      $this->post->edit($id, $title_english, $content_english, $title_arabic, $content_arabic, $image);

      return response()->json(['success' => 1], 200);
    }

    public function delete($id)
    {
      $this->post->find($id)->delete();

      return response()->json(['success' => 1], 200);
    }
}
